﻿using UnityEngine;
using System.Collections;

public class fading : MonoBehaviour
{
    public float op = 0f;
    public float op1 = 0f;
    public float op2 = 0f;
  
   public bool shoulddo=true;

    void Start()
    {
        foreach (Material item in mat2)
        {
            item.shader = Shader.Find("Transparent/Specular");
        }

        increamentvalue();
    }
    public Material[] mat;
    public Material[] mat1;
    public Material[] mat2;
    public GameObject layer0;
    public GameObject layer;
    public GameObject layer2;
    string text;
    void Update()
    {
        for (int i = 0; i < mat.Length; i++)
        {
            mat[i].color = new Color(mat[i].color.r, mat[i].color.g, mat[i].color.b, op);
        }
        for (int i = 0; i < mat1.Length; i++)
        {
            mat1[i].color = new Color(mat1[i].color.r, mat1[i].color.g, mat1[i].color.b, op1);
        }
        for (int i = 0; i < mat2.Length; i++)
        {
            mat2[i].color = new Color(mat2[i].color.r, mat2[i].color.g, mat2[i].color.b, op2);
        }

    }
    private float screenX;
    private float screenY;

    void Awake()
    {
        screenX = Screen.width;
        screenY = Screen.height;
    }
   
    public GameObject finishrotation;
    public GameObject finishchildrotation;
    private GUIStyle buttonstyle;

    bool isRuined;
    float buttonwidth;

    void OnGUI()
    {
        screenX = Screen.width;
        screenY = Screen.height;

        if (Screen.width >= Screen.height)
        {
            GUI.skin.button.fontSize = (int)Mathf.Round(12 * Screen.width / 480);
            GUI.skin.box.fontSize = (int)Mathf.Round(12 * Screen.width / 320);
            buttonwidth = Screen.width / 5f;
        }
        else
        {
            GUI.skin.button.fontSize = (int)Mathf.Round(12 * Screen.height / 480);
            GUI.skin.box.fontSize = (int)Mathf.Round(12 * Screen.height / 320);
            buttonwidth = Screen.height / 5f;
        }

        buttonstyle = GUI.skin.GetStyle("Button");
        buttonstyle.normal.background = Resources.Load("btn3") as Texture2D;
        buttonstyle.hover.background = Resources.Load("btn3") as Texture2D;
        buttonstyle.normal.textColor = Color.white;      
        buttonstyle.alignment = TextAnchor.MiddleCenter;
        if (GUI.Button(new Rect(0, 0,buttonwidth, screenY / 12), new GUIContent("Stop Animation"), buttonstyle))
        {
            shoulddo = false;
            op=1;
            op1 = 0.4f;
            op2 = 1.2f;
            layer0.SetActive(true);
            layer.SetActive(true);
            layer2.SetActive(true);
            GameObject cam= GameObject.Find("Main Camera");
            cam.camera.fieldOfView = 57.5f;         
            cam.transform.position = finishrotation.transform.position;
            cam.transform.rotation = finishrotation.transform.rotation;           
            cam.transform.GetChild(0).gameObject.transform.localRotation = finishchildrotation.transform.rotation;         
            Destroy(GameObject.Find("Main Camera").GetComponent<freerotationcamera>());
            GameObject.Find("Main Camera").AddComponent<Transitions>();
            
        }


    }
   
    void increamentvalue()
    {
      
        if (shoulddo)
        {
            if (layer != null && layer2 != null)
            {
                if (layer.activeInHierarchy == false && layer2.activeInHierarchy == false)
                {
                    if (op <= 1)
                    {
                        op += 0.1f;

                    }
                    else
                    {

                        layer.SetActive(true);
                    }

                }
                else if (layer2.activeInHierarchy == false)
                {
                    if (op1 <= 0.4)
                    {
                        op1 += 0.1f;

                    }
                    else
                    {

                        layer2.SetActive(true);

                    }
                }
                else if (layer.activeInHierarchy == true && layer2.activeInHierarchy == true)
                {
                    if (op2 <= 1.5)
                    {
                        op2 += 0.15f;                      

                    }
                    else
                    {
                        
                        layer.SetActive(false);
                        layer0.SetActive(false);                      

                    }
                   
                }
               
                Invoke("increamentvalue", 0.3f);

            }
        }
    }
}

