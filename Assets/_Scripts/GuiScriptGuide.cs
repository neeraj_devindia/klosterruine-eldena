﻿using UnityEngine;
using System.Collections;

public class GuiScriptGuide : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        text[0] = @"Modell
                     In this mode user can look arround the model and there are diffrent view point on which user can click and so to a specific location where he can see the 360 view from there";

        text[1] = @"Explore
                     In this mode user will navigate to scpecific location with the help of images.On that location he can see 360 view.";
        text[2] = @"History
                     In this mode user can get some information about the Kloster. he can also see the images of the kloster.";
        text[3] = @"setting
                     In this mode user can set wheather he want landscape mode or horizontal mode. he can turn off and on the sound.";
    }
    private float screenX;
    private float screenY;

    void Awake()
    {
        screenX = Screen.width;
        screenY = Screen.height;
    }
    // Update is called once per frame
    void Update()
    {

    }
    public Texture2D image;
    public Texture2D image2;
    public Texture2D Next;
    public Texture2D Previous;
    public Texture2D close;
    string[] text = new string[4];
    private GUIStyle buttonstyle;
    int currentpage = 0;
    public bool Dispayintro = false;

    void OnGUI()
    {
        screenX = Screen.width;
        screenY = Screen.height;
        GUIStyle style = new GUIStyle();
        if (Screen.width >= Screen.height)
        {
            GUI.skin.button.fontSize = (int)Mathf.Round(12 * Screen.width / 480);
            style.fontSize = (int)Mathf.Round(12 * Screen.width / 320);
        }
        else
        {
            GUI.skin.button.fontSize = (int)Mathf.Round(12 * Screen.height / 480);
            style.fontSize = (int)Mathf.Round(12 * Screen.height / 320);
        }

        buttonstyle = GUI.skin.GetStyle("Button");
        buttonstyle.normal.textColor = Color.white;

        style.alignment = TextAnchor.MiddleLeft;
        style.wordWrap = true;
        style.normal.textColor = Color.black;

        if (false)
        {

            GUI.DrawTexture(new Rect(screenX * 0.1f, screenY * .05f, screenX - screenX * .2f, screenY - screenY * .3f), image);
            GUI.Box(new Rect(screenX * 0.1f, screenY * .05f, screenX - screenX * .2f, screenY - screenY * .3f), text[currentpage], style);
            if (GUI.Button(new Rect(screenX * .85f, screenY * .06f, screenX * .05f, screenY * 0.05f), close, new GUIStyle()))
            {
                Dispayintro = false;
            }
            GUI.Button(new Rect(0, screenY - screenY / 8, .5F * (Screen.width / 4f), screenY / 8), "", new GUIStyle());
            GUI.Button(new Rect(3.5f * screenX / 4, screenY - screenY / 8, .5F * (Screen.width / 4f), screenY / 8), "", new GUIStyle());
            GUI.Button(new Rect(1f * screenX / 4, screenY - screenY / 8, 2F * (screenX / 4f), screenY / 8), "", new GUIStyle());

            if (currentpage < 3)
            {

                if (GUI.Button(new Rect(screenX * .85f, screenY * .69f, screenX * .05f, screenY * 0.05f), Next, new GUIStyle()))
                {
                    currentpage++;
                }
            }
            else
            {
                if (GUI.Button(new Rect(screenX * .85f, screenY * .69f, screenX * .05f, screenY * 0.05f), "", new GUIStyle()))
                {
                }

            }
            if (currentpage > 0)
            {
                if (GUI.Button(new Rect(screenX * .15f, screenY * .69f, screenX * .05f, screenY * 0.05f), Previous, new GUIStyle()))
                {
                    currentpage--;
                }
            }
            else
            {
                if (GUI.Button(new Rect(screenX * .15f, screenY * .69f, screenX * .05f, screenY * 0.05f), "", new GUIStyle()))
                {

                }
            }

        }
    }
}
