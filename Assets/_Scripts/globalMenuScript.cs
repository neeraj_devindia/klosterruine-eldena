﻿using System.Collections.Generic;
using UnityEngine;

public class globalMenuScript : MonoBehaviour
{
    private float screenX;
    private float screenY;

    public string btnName = "Mini Map OFF";
    // Use this for initialization
    Camera[] cm;
    private Material[] skyMat;
    GameObject link1, link2, link3, link4, link5,link6;

    public AudioClip aud1;
    void Awake()
    {
        screenX = Screen.width;
        screenY = Screen.height;
       // audio.clip = Resources.Load("Sounds") as AudioClip;
    }
    void Start()
	{        
        skyMat = new Material[6];
        for (int i = 0; i < skyMat.Length; i++)
        {
            skyMat[i] = (Material)Resources.Load(("Skybox_gr" + (i + 1)));
        }
        cm = Camera.allCameras;
        link1 = GameObject.Find("ReadMark1");
        link2 = GameObject.Find("ReadMark2");
        link3 = GameObject.Find("ReadMark3");
        link4 = GameObject.Find("ReadMark4");
        link5 = GameObject.Find("ReadMark5");
        link6 = GameObject.Find("ReadMark6");
        //audio.clip = Resources.Load("Sounds") as AudioClip;
      //  audio.clip = aud1;      
        audio.clip = Resources.Load("Audio/1/Zur Baugeschichte des Klosters") as AudioClip;
        audio.Play();
        //audio.clip = clip;

        switch (Clickable.selected_SkyName)
        {

            case "Number1":
                {
                    audio.clip = Resources.Load("Audio/sk1/Die Klostergründung 1") as AudioClip;
                    audio.Play();
                    break;
                }
            case "Number2":
                {
                    audio.clip = Resources.Load("Audio/1/Zur Baugeschichte des Klosters") as AudioClip;
                    audio.Play();
                    break;
                }
            case "Number3":
                {
                    audio.clip = Resources.Load("Audio/2/Die Klosterkirche as herzogliche Grablege") as AudioClip;
                    audio.Play();
                    break;
                }
            case "Number4":
                {

                    audio.clip = Resources.Load("Audio/3/Die Klosterruine as Symbol der Romantik") as AudioClip;
                    audio.Play();

                    break;
                }
            case "Number5":
                {
                    audio.clip = Resources.Load("Audio/1/Zur Baugeschichte des Klosters") as AudioClip;
                    audio.Play();
                    break;
                }
            case "Number6":
                {
                    audio.clip = Resources.Load("Audio/3/Die Klosterruine as Symbol der Romantik") as AudioClip;
                    audio.Play();
                    break;
                }
            default:
                {
                    audio.clip = Resources.Load("Audio/3/Die Klosterruine as Symbol der Romantik") as AudioClip;
                    audio.Play();
                    break;
                }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
   
   
    public Texture2D image;
    bool draw = false;
    GUIStyle label;
    string abc = "Loading...";
    public bool boolToogleButton;
    public GameObject wind;
    void OnGUI()
    {
        label = GUI.skin.GetStyle("Label");
        label.fontSize = (int)Mathf.Round(10 * Screen.width / 320);
       // label.font = Resources.Load("FuturaBK_WindowText") as Font;
        if (draw)
        {
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), image);
            GUI.Label(new Rect((Screen.width / 2) - 130, Screen.height / 2, 600, 60), abc);
        }
        if (true)
        {
            screenX = Screen.width;
            screenY = Screen.height;
            GUI.skin.box.alignment = TextAnchor.MiddleCenter;
         //`   GUI.skin.box.font = Resources.Load("FuturaBK_WindowText") as Font;
            GUI.skin.box.normal.background = Resources.Load("grey") as Texture2D;
            if (Screen.width >= Screen.height)
            {
                GUI.skin.button.fontSize = (int)Mathf.Round(10 * Screen.width / 480);
                GUI.skin.box.fontSize = (int)Mathf.Round(10 * Screen.width / 320);
            }
            else
            {
                GUI.skin.button.fontSize = (int)Mathf.Round(10 * Screen.height / 480);
                GUI.skin.box.fontSize = (int)Mathf.Round(10 * Screen.height / 320);
            }


            



            /*
            if (GUI.Button(new Rect(0, screenY - screenY / 12, screenX / 5, screenY / 12), "Go to Map"))
            {
                // MouseLook.isRotate = false;
                MinimalSensorCamera.isRotate = false;
                MinimalSensorCamera.isInitialize = true;
               // UserOption.isMini_Map = false;
            }
            if (GUI.Button(new Rect(screenX / 5, screenY - screenY / 12, screenX / 5, screenY / 12), "History"))
            {
                Application.LoadLevel("scn_Info");
            }
            if (GUI.RepeatButton(new Rect(2 * screenX / 5, screenY - screenY / 12, screenX / 5, screenY / 12), "Locations"))
            {
                if (Input.GetMouseButton(0))
                {
                    Application.LoadLevel("scn_LocationMenu");

                }
            }


             * 

            //Update map and center user position 


            if (GUI.Button(new Rect(3 * screenX / 5, screenY - screenY / 12, screenX / 5, screenY / 12), "Restart"))
            {
               // UserOption.isMini_Map = false;
                Application.LoadLevel("scn_Welcome");
            }

            //Show GPS Status info. Please make sure the GPS_Status.cs script is attached and enabled in the map object.
            if (GUI.Button(new Rect(4 * screenX / 5, screenY - screenY / 12, screenX / 5, screenY / 12), "Quit"))
            {
                Application.Quit();
            }
            //			GUI.EndGroup ();

            */
            //________________Navigation Nos_________________________________________
            /*
            if (GUI.Button(new Rect(0 * screenX / 5, 0f, screenX / 10, screenY / 12), "1"))
            {
                //MouseLook.isRotate = false;  //Enbable when character package added
                MinimalSensorCamera.isRotate = false;
                MinimalSensorCamera.isInitialize = true;
                RenderSettings.skybox = skyMat[0];
                link1.SetActive(true);
                link2.SetActive(false);
                link3.SetActive(false);
                link4.SetActive(false);
                link5.SetActive(false);
                link6.SetActive(false);

                audio.clip = Resources.Load("Audio//") as AudioClip;
                audio.Play();
            }
            if (GUI.Button(new Rect(0.5f * screenX / 5, 0f, screenX / 10, screenY / 12), "2"))
            {
                // MouseLook.isRotate = false;
                MinimalSensorCamera.isRotate = false;
                MinimalSensorCamera.isInitialize = true;
                RenderSettings.skybox = skyMat[1];
                link1.SetActive(false);
                link2.SetActive(true);
                link3.SetActive(false);
                link4.SetActive(false);
                link5.SetActive(false);
                link6.SetActive(false);
                audio.clip = Resources.Load("Audio//") as AudioClip;
                audio.Play();
               
            }
            if (GUI.Button(new Rect(1f * screenX / 5, 0f, screenX / 10, screenY / 12), "3"))
            {
              //  MouseLook.isRotate = false;
                MinimalSensorCamera.isRotate = false;
                MinimalSensorCamera.isInitialize = true;
                RenderSettings.skybox = skyMat[2];
                link1.SetActive(false);
                link2.SetActive(false);
                link3.SetActive(true);
                link4.SetActive(false);
                link5.SetActive(false);
                link6.SetActive(false);
                audio.clip = Resources.Load("Audio//") as AudioClip;
                audio.Play();
                
            }
            if (GUI.Button(new Rect(1.5f * screenX / 5, 0f, screenX / 10, screenY / 12), "4"))
            {
                // MouseLook.isRotate = false;
                MinimalSensorCamera.isRotate = false;
                MinimalSensorCamera.isInitialize = true;
                RenderSettings.skybox = skyMat[3];
                link1.SetActive(false);
                link2.SetActive(false);
                link3.SetActive(false);
                link4.SetActive(true);
                link5.SetActive(false);
                link6.SetActive(false);


                
            }
            if (GUI.Button(new Rect(2f * screenX / 5, 0f, screenX / 10, screenY / 12), "5"))
            {
                //  MouseLook.isRotate = false;
                MinimalSensorCamera.isRotate = false;
                MinimalSensorCamera.isInitialize = true;
                RenderSettings.skybox = skyMat[4];
                link1.SetActive(false);
                link2.SetActive(false);
                link3.SetActive(false);
                link4.SetActive(false);
                link5.SetActive(true);
                link6.SetActive(false);
                
            }
            if (GUI.Button(new Rect(2.5f * screenX / 5, 0f, screenX / 10, screenY / 12), "6"))
            {
                //  MouseLook.isRotate = false;
                MinimalSensorCamera.isRotate = false;
                MinimalSensorCamera.isInitialize = true;
                RenderSettings.skybox = skyMat[5];
                link1.SetActive(false);
                link2.SetActive(false);
                link3.SetActive(false);
                link4.SetActive(false);
                link5.SetActive(false);
                link6.SetActive(true);
                
            }*/

            
            /*
            if (boolToogleButton == false)
            {
                if (GUI.Button(new Rect(2.5f * screenX / 5, 0f, screenX / 10 + 200f, screenY / 12), "Mini Map-OFF")) // MyGUISkin.customStyles[1] is unselected button image
                {
                    boolToogleButton = true;
                    
                    MinimalSensorCamera.isRotate = false;
                    
                   // UserOption.showMiniMap = false;
                }
            }
            else
            {
                if (GUI.Button(new Rect(2.5f * screenX / 5, 0f, screenX / 10 + 200f, screenY / 12), "Mini Map-ON")) // MyGUISkin.customStyles[2] is selected button image
                {
                    boolToogleButton = false;
                   
                    MinimalSensorCamera.isRotate = false;

                   // UserOption.showMiniMap = true;
                }
            } 
            */
        }

     
    }


}

