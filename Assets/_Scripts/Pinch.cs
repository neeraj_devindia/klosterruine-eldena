﻿using UnityEngine;
using System.Collections;

public class Pinch : MonoBehaviour
{
            // The rate of change of the field of view in perspective mode.
    public float orthoZoomSpeed = 0.5f;        // The rate of change of the orthographic size in orthographic mode.

	public float perspectiveZoomSpeed = 0.5f;

    public void Awake()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }


    private bool isPort = true;
    private bool isLand = false;

    private bool isSound = true;
    public Texture2D optionImg;
    public bool isPortrait = true;
    void Start()
    {

        optionImg = Resources.Load("rotate-icon") as Texture2D;
        isSound = System.Convert.ToBoolean(PlayerPrefs.GetInt("isSound"));

        isPort = System.Convert.ToBoolean(PlayerPrefs.GetInt("isPort"));

    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }

        if (isPort)
            Screen.orientation = ScreenOrientation.Portrait;
        else
            Screen.orientation = ScreenOrientation.Landscape; 

        // If there are two touches on the device...
        if (Input.touchCount == 2)
        {
            // Store both touches.
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            // Find the position in the previous frame of each touch.
            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            // Find the magnitude of the vector (the distance) between the touches in each frame.
            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            // Find the difference in the distances between each frame.
            float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

			/*	 transform.Translate(0f,0f,-deltaMagnitudeDiff*0.01f);  
			if(transform.position.z>-4.15f)
			{
				transform.Translate(0f,0f,+deltaMagnitudeDiff*0.01f); 
			}
*/

            // If the camera is orthographic...
            if (camera.isOrthoGraphic)
            {
                // ... change the orthographic size based on the change in distance between the touches.
                camera.orthographicSize += deltaMagnitudeDiff * orthoZoomSpeed;

                // Make sure the orthographic size never drops below zero.
                camera.orthographicSize = Mathf.Max(camera.orthographicSize, 0.1f);
            }
            else
            {
                if (true)
                {
                // Otherwise change the field of view based on the change in distance between the touches.
                camera.fieldOfView += deltaMagnitudeDiff * perspectiveZoomSpeed;

                // Clamp the field of view to make sure it's between 0 and 180.
                camera.fieldOfView = Mathf.Clamp(camera.fieldOfView, 15f, 100f);
                
                }
            }
            
            
        }
        
    }

    void OnGUI()
    {
        if (isSound)
        {
            optionImg = Resources.Load("sound") as Texture2D;
            AudioListener.pause = false;
        }
        else
        {
            optionImg = Resources.Load("mute") as Texture2D;
            AudioListener.pause = true;
        }
        if (GUI.Button(new Rect((Screen.width - optionImg.width) - 10f, 5f, optionImg.width, optionImg.height), "", new GUIStyle()))
        {
            isSound = !isSound;
            PlayerPrefs.SetInt("isSound", System.Convert.ToInt16(isSound));
        }
        GUI.DrawTexture(new Rect((Screen.width - optionImg.width) - 10f, 5f, optionImg.width, optionImg.height), optionImg);
    }
}