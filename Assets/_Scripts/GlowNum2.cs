﻿using UnityEngine;
using System.Collections;

public class GlowNum2 : MonoBehaviour
{
    public float smooth = 5.3f;
    private Color newColour;
    void Awake()
    {
        newColour = light.color;
    }

    float timespan = 0;
    void Update()
    {
        timespan = timespan + Time.deltaTime;
        ColourChanging();
    }

    void ColourChanging()
    {
        Color colourA = Color.red;
        Color colourB = Color.green;

        if (Input.GetKeyDown(KeyCode.Z))
            newColour = colourA;
        if (Input.GetKeyDown(KeyCode.C))
            newColour = colourB;

        if (timespan > 2 && timespan < 4)
        {
            newColour = Color.cyan;
            if (System.Math.Round(timespan) == 4)
            {
                timespan = 0;
            }
        }
        else
        {
            newColour = Color.red;
        }

        light.color = Color.Lerp(light.color, newColour, smooth * Time.deltaTime);

    }
    void Start()
    {

    }
}
