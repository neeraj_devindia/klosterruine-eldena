﻿using UnityEngine;
using System.Collections;

public class test : MonoBehaviour {

	// Use this for initialization
    string msg;
    Vector2 scrollPosition;
	void Start () {
        scrollPosition = Vector2.zero;
        msg= @"<color=red>Wege zum Kloster</color>Die Klosterruine Eldena liegt etwa 5 km von der Altstadt entfernt im Osten der Universität- und Hansestadt Greifswald nahe der Mündung des Ryck in die Dänische Wiek. Man erreicht sie auf dem Weg entlang des Ryck zum ehemaligen Fischerdorf Wieck bequem zu Fuß oder mit dem Fahrrad. Mit Bus oder Auto geht es in Richtung Lubmin und Wolgast zum Ortsteil Eldena. Auch eine etwa halbstündige Schiffspartie mit der „Stubnitz vom Altstadthafen bis zur Anlegestelle in Wieck lohnt sich. Über den „Studentensteig ist Eldena von dort in wenigen Minuten zu Fuß zu erreichen. Segler können auch über den Greifswalder Bodden im Wiecker Hafen einlaufen, um von hier aus die Klosterruine als einen ganz besonderen Ort unter den Klosterstätten in Mecklenburg-Vorpommern zu besuchen.
Durch den in Greifswald geborenen Maler Caspar David Friedrich ist die Ruine des ehemaligen Zisterzienserklosters zu einem Sinnbild der romantischen Malerei geworden. Zu seinen Lebzeiten befanden sich das Klostergelände und die Ländereien im Besitz der Universität, die das Amt Eldena vom letzten Pommernherzog Bogislaw XIV. im Jahr 1634 erhalten hatte. Aus der nachreformatorischen Geschichte des Klosters ist der Ausbau des Amtsgutes und die Gründung eine r Landwirtschaftsakademie 1835 hervorzuheben, die bis 1876 bestand. Die noch vorhandenen Klostergebäude wurden ebenfalls landwirtschaftlich genutzt. Seit 1937 im Besitz der Stadt Greifswald, ist die Klosterruine im Sommer Theaterspielort und bietet alljährlich am ersten Juliwochenende die Kulisse für die Eldenaer Jazz Evenins. Die Klosterruine ist Station auf dem Caspar-David- Friedrich-Bildweg und der Europäischen Route der Backsteingotik.";

	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnGUI()
    {
      //  scrollPosition = GUI.BeginScrollView(new Rect(10, (Screen.height - (Screen.height * 0.12f)) / 2, Screen.width, Screen.height / 2), scrollPosition, new Rect(0, 0, Screen.width * 0.9f, 0));

        GUIStyle style = new GUIStyle();
        style.richText = true;
        style.wordWrap = true;
        
        style.fontSize = (int)(Screen.width * 0.05f);
        //style.normal.textColor = Color.black;
        GUILayout.Label(msg, style);

        // GUI.Label(new Rect(15, labelposition, Screen.width * 0.9f, 4f * Screen.height), textused[index],labelstyle);
     //   GUI.EndScrollView();
    }
}
