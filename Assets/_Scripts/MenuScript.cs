﻿using UnityEngine;
using System.Collections;

public class MenuScript : MonoBehaviour
{
    Texture2D Historyimage;
    Texture2D Exploreimage;
    Texture2D ARimage;
    Texture2D Settingimage;
    private float height;
    private float width;
    private Font font;
    // Use this for initialization

    private float screenX;
    private float screenY;

    void Awake()
    {
        screenX = Screen.width;
        screenY = Screen.height;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }
    void Start()
    {
        height = Screen.height / 8;
        width = Screen.width / 5;
        // image = Resources.Load("Images/Menu") as Texture2D;
        font = Resources.Load("FuturaBK_WindowText") as Font;
        //image = Resources.Load("Images/menu") as Texture2D;
        Historyimage = Resources.Load("HistoryImage") as Texture2D;
        ARimage = Resources.Load("CameraImage") as Texture2D;
        Settingimage = Resources.Load("SettingImage") as Texture2D;
        Exploreimage = Resources.Load("ViewImage") as Texture2D;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private GUIStyle buttonstyle;
    private GUIStyle labelstyle;

    void OnGUI()
    {
        screenX = Screen.width;
        screenY = Screen.height;

        if (Screen.width >= Screen.height)
        {
            GUI.skin.button.fontSize = (int)Mathf.Round(12 * Screen.width / 480);
            GUI.skin.box.fontSize = (int)Mathf.Round(12 * Screen.width / 320);
        }
        else
        {
            GUI.skin.button.fontSize = (int)Mathf.Round(12 * Screen.height / 480);
            GUI.skin.box.fontSize = (int)Mathf.Round(12 * Screen.height / 320);
        }

        buttonstyle = GUI.skin.GetStyle("Button");
        buttonstyle.normal.background = Resources.Load("btn3") as Texture2D;
        buttonstyle.hover.background = Resources.Load("btn3") as Texture2D;
        buttonstyle.normal.textColor = Color.white;
        labelstyle = GUI.skin.GetStyle("Label");

        labelstyle.font = font;
        //buttonstyle.font = font;
        //buttonstyle.fontSize = (int)((Screen.width + Screen.height) * 0.026f);

        //labelstyle.fontSize = (int)(Screen.width * 0.02f);
        buttonstyle.alignment = TextAnchor.LowerCenter;
        buttonstyle.imagePosition = ImagePosition.ImageAbove;


        //GUI.skin.button.normal.background = (Texture2D)content.image;
        //GUI.skin.button.hover.background = (Texture2D)content.image;
        //GUI.skin.button.active.background = (Texture2D)content.image;

        if (GUI.Button(new Rect(0, screenY - screenY / 8, screenX / 4f, screenY / 8), new GUIContent("Modell", Exploreimage), buttonstyle))
        {
            Application.LoadLevel("scn_Map");
        }

        if (GUI.Button(new Rect(screenX / 4, screenY - screenY / 8, screenX / 4f, screenY / 8), new GUIContent("Entdecken", ARimage), buttonstyle))
        {
            Application.LoadLevel("scn_Explore");
        }
        if (GUI.Button(new Rect(2 * screenX / 4, screenY - screenY / 8, screenX / 4f, screenY / 8), new GUIContent("Geschichte", Historyimage), buttonstyle))
        {

            Application.LoadLevel("scn_History");
        }
        if (GUI.Button(new Rect(3 * screenX / 4, screenY - screenY / 8, screenX / 4f, screenY / 8), new GUIContent("Einstellungen", Settingimage), buttonstyle))
        {
            Application.LoadLevel("scn_Setting");
        }

        if (Input.GetKey(KeyCode.Escape))
        {
            Application.LoadLevel("scn_Map");
        }

        //--------------------------------------------------------------------------------------------------------------------------
 /*
        if (GUI.Button(new Rect(0, screenY - screenY / 8, screenX / 4f, screenY / 8), "Go to Map"))
        {
         
        }
        if (GUI.Button(new Rect(screenX / 4, screenY - screenY / 8, screenX / 4f, screenY / 8), "History"))
        {
            
        }
        if (GUI.RepeatButton(new Rect(2 * screenX / 4, screenY - screenY / 8, screenX / 4f, screenY / 8), "Locations"))
        {
           
        }

        if (GUI.Button(new Rect(3 * screenX / 4, screenY - screenY / 8, screenX / 4f, screenY / 8), "Restart"))
        {
           
        }

        //Show GPS Status info. Please make sure the GPS_Status.cs script is attached and enabled in the map object.
       if (GUI.Button(new Rect(4 * screenX / 5, screenY - screenY / 12, screenX / 5, screenY / 8), "Quit"))
        {
            
        }*/

        /*
        if (GUI.Button(new Rect(Screen.width - (5 * width), Screen.height - (height * 1f), width, height), new GUIContent("Free Mode", image), buttonstyle))
        {
            Application.LoadLevel("scn_Map");
        }

        if (GUI.Button(new Rect(Screen.width - (4 * width), Screen.height - (height * 1f), width, height), new GUIContent("Explore", image), buttonstyle))
        {
            Application.LoadLevel("scn_Explore");
        }
        if (GUI.Button(new Rect(Screen.width - (3 * width), Screen.height - (height * 1f), width, height), new GUIContent("History", image), buttonstyle))
        {

            Application.LoadLevel("scn_History");
        }
        if (GUI.Button(new Rect(Screen.width - (2 * width), Screen.height - (height * 1f), width, height), new GUIContent("Setting", image), buttonstyle))
        {
            Application.LoadLevel("scn_Setting");
        }

        if (GUI.Button(new Rect(Screen.width - (1 * width), Screen.height - (height * 1f), width, height), new GUIContent("Exit", image), buttonstyle))
        {
            Application.Quit();
        }
         * */

    }
}
