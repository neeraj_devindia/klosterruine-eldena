﻿using System.Collections.Generic;
using UnityEngine;

public class Load_Skybox : MonoBehaviour
{
    private Material[] skyMat;
    public Texture imgArrow;
    public Texture imgArrowLeft;
    public static bool isSound = true;
    GameObject link1, link2, link3, link4, link5,link6;

    public Texture2D optionImg;
    public bool isPortrait = true;

    void Start()
    {

        isPortrait = System.Convert.ToBoolean(PlayerPrefs.GetInt("isPort"));

        if (isPortrait)
            Screen.orientation = ScreenOrientation.Portrait;
        else
            Screen.orientation = ScreenOrientation.Landscape;

        audio.clip = Resources.Load("sounds") as AudioClip;
       // Screen.orientation = ScreenOrientation.Portrait;
        isSound = System.Convert.ToBoolean(PlayerPrefs.GetInt("isSound"));

        link1 = GameObject.Find("ReadMark1");
        link2 = GameObject.Find("ReadMark2");
        link3 = GameObject.Find("ReadMark3");
        link4 = GameObject.Find("ReadMark4");
        link5 = GameObject.Find("ReadMark5");
        link6 = GameObject.Find("ReadMark6");

        skyMat = new Material[6];

		print (Clickable.selected_SkyName);

        for (int i = 0; i < skyMat.Length; i++)
        {
            skyMat[i] = (Material)Resources.Load(("Skybox_gr" + (i + 1)));

        }
		switch (Clickable.selected_SkyName)
        {

            case "Number1":
                {
                    RenderSettings.skybox = skyMat[0];
                    link1.SetActive(true);
                    link2.SetActive(false);
                    link3.SetActive(false);
                    link4.SetActive(false);
                    link5.SetActive(false);
                    link6.SetActive(false);
                    break;
                }
            case "Number2":
                {
                    RenderSettings.skybox = skyMat[1];
                    link1.SetActive(false);
                    link2.SetActive(true);
                    link3.SetActive(false);
                    link4.SetActive(false);
                    link5.SetActive(false);
                    link6.SetActive(false);
                    break;
                }
            case "Number3":
                {
                    RenderSettings.skybox = skyMat[2];
                    link1.SetActive(false);
                    link2.SetActive(false);
                    link3.SetActive(true);
                    link4.SetActive(false);
                    link5.SetActive(false);
                    link6.SetActive(false);
                    break;
                }
            case "Number4":
                {
                    RenderSettings.skybox = skyMat[3];
                    link1.SetActive(false);
                    link2.SetActive(false);
                    link3.SetActive(false);
                    link4.SetActive(true);
                    link5.SetActive(false);
                    link6.SetActive(false);

                    break;
                }
            case "Number5":
                {
                    RenderSettings.skybox = skyMat[4];
                    link1.SetActive(false);
                    link2.SetActive(false);
                    link3.SetActive(false);
                    link4.SetActive(false);
                    link5.SetActive(true);
                    link6.SetActive(false);
                    break;
                }
            case "Number6":
                {
                    RenderSettings.skybox = skyMat[5];
                    link1.SetActive(false);
                    link2.SetActive(false);
                    link3.SetActive(false);
                    link4.SetActive(false);
                    link5.SetActive(false);
                    link6.SetActive(true);
                    break;
                }
            default:
                {
                    RenderSettings.skybox = skyMat[0];
                    link1.SetActive(true);
                    link2.SetActive(false);
                    link3.SetActive(false);
                    link4.SetActive(false);
                    link5.SetActive(false);
                    link6.SetActive(false);
                    break;
                }
        }
       
    }

    void Update()
    {
       /* isPortrait = System.Convert.ToBoolean(PlayerPrefs.GetInt("isPort"));
*/
        if (isPortrait)
            Screen.orientation = ScreenOrientation.Portrait;
        else
            Screen.orientation = ScreenOrientation.Landscape; 
        
    }

    GUIStyle label;

    void OnGUI()
    {
        label = GUI.skin.GetStyle("Label");
        label.fontSize = (int)Mathf.Round(10 * Screen.width / 290);
        label.normal.textColor = Color.red;

        Vector3 v3 = Input.acceleration;

        
            if (Input.deviceOrientation==DeviceOrientation.FaceUp)
            {
                GUI.DrawTexture(new Rect(Screen.width / 2f, Screen.height - (Screen.height * 0.7f), 100f, 100f), imgArrow, ScaleMode.ScaleToFit);
                GUI.Label(new Rect(Screen.width / 2.5f, Screen.height - (Screen.height * 0.6f), 1000f, 100f), "Bitte nach oben schauen");
            }
        
        //________________________________________________________________________________________________________________________________________
        if (isSound)
        {
            optionImg = Resources.Load("sound") as Texture2D;
            AudioListener.pause = false;            
        }
        else
        {
            optionImg = Resources.Load("mute") as Texture2D;
            AudioListener.pause = true;
        }
        if (GUI.Button(new Rect((Screen.width - optionImg.width)-10f, 5f, optionImg.width, optionImg.height), "",new GUIStyle()))
        {
            isSound = !isSound;
            PlayerPrefs.SetInt("isSound", System.Convert.ToInt16(isSound));
        }
        GUI.DrawTexture(new Rect((Screen.width - optionImg.width) - 10f, 5f, optionImg.width, optionImg.height), optionImg);

    }
}
