﻿using UnityEngine;
using System.Collections;

public class Settings : MonoBehaviour
{

    private float screenX;
    private float screenY;
    GUIStyle lblStyle;

    public GUISkin testGUIskin;

    private bool isTest = true;
    private bool isMove = false;

    private static bool isPort = true;
    private static bool isLand = false;

    void Awake()
    {
        screenX = Screen.width;
        screenY = Screen.height;
    }

    private bool isSound = true;
    private bool isTutorial = true;
    public Texture2D optionImg;
    public bool isPortrait = true;
    void Start()
    {
        optionImg = Resources.Load("rotate-icon") as Texture2D;
        isSound = System.Convert.ToBoolean(PlayerPrefs.GetInt("isSound"));

        lblStyle = new GUIStyle();
        lblStyle.normal.textColor = Color.black;
        
       // isRotate = System.Convert.ToBoolean(PlayerPrefs.GetInt("isRotate"));        
        isPort = System.Convert.ToBoolean(PlayerPrefs.GetInt("isPort"));
        isTutorial = System.Convert.ToBoolean(PlayerPrefs.GetInt("isTutorial"));

    }

    void Update()
    {
     
    }
    public void RotatScr()
    {
        if (isPort)
            Screen.orientation = ScreenOrientation.Portrait;
        else
            Screen.orientation = ScreenOrientation.Landscape; 
    }

    GUIStyle label;
    bool toggleSound = true;
    int labelfont;
    void OnGUI()
    {

        if (Screen.height < Screen.width)
        {
            labelfont = (int)(Screen.height * 0.05f);
        }
        else
        {
            labelfont = (int)(Screen.width * 0.05f);
        }
        label = GUI.skin.GetStyle("Label");
        label.fontSize = labelfont;
        label.font = Resources.Load("FuturaBK_WindowText") as Font;
        label.fontStyle = FontStyle.Bold;

        screenX = Screen.width;
        screenY = Screen.height;
        GUI.skin.box.alignment = TextAnchor.MiddleCenter;
       // GUI.skin.box.font = Resources.Load("FuturaBK_WindowText") as Font;
        GUI.skin.box.normal.background = Resources.Load("red") as Texture2D;

        lblStyle.fontSize = labelfont;

        if (Screen.width >= Screen.height)
        {
            GUI.skin.button.fontSize = (int)Mathf.Round(10 * Screen.width / 480);
            GUI.skin.box.fontSize = (int)Mathf.Round(10 * Screen.width / 320);
        }
        else
        {
            GUI.skin.button.fontSize = (int)Mathf.Round(10 * Screen.height / 480);
            GUI.skin.box.fontSize = (int)Mathf.Round(10 * Screen.height / 320);
        }

        GUIStyle lblStyle2 = new GUIStyle();
        lblStyle2.normal.textColor = Color.gray;

        GUI.Label(new Rect(100f, screenY - screenY / 1.02f, screenX / 5, screenY / 12), "Ton", lblStyle);        
        isSound = GUI.Toggle(new Rect(screenX - screenX / 2.5f, screenY - screenY / 1.02f, 64, 64), this.isSound, "", this.testGUIskin.customStyles[0]);


        GUI.Label(new Rect(100f, screenY - screenY / 1.18f, screenX / 5, screenY / 12), "Hochformat", lblStyle);
        if (GUI.Button(new Rect(screenX - screenX / 2.5f, screenY - screenY / 1.18f, 64, 64), "",new GUIStyle()))
        {
            isPort = true;
            if (isPort)
            {
                Screen.orientation = ScreenOrientation.Portrait;           
            }
            else
            {
                Screen.orientation = ScreenOrientation.Landscape;           
            }
            print(Screen.orientation.ToString());
        }
        isTest = GUI.Toggle(new Rect(screenX - screenX / 2.5f, screenY - screenY / 1.18f, 64, 64), isPort, "", this.testGUIskin.customStyles[0]);
        
        GUI.Label(new Rect(100f, screenY - screenY / 1.39f, screenX / 5, screenY / 12), "Querformat", lblStyle);

       if (GUI.Button(new Rect(screenX - screenX / 2.5f, screenY - screenY / 1.39f, 64, 64), "", new GUIStyle()))
        {
            isPort = false;
            if (isPort)
            {
                Screen.orientation = ScreenOrientation.Portrait;
            }
            else
            {
                Screen.orientation = ScreenOrientation.Landscape;
            }
            Screen.orientation = ScreenOrientation.Landscape;
            print(Screen.orientation.ToString());
        }
       isTest = GUI.Toggle(new Rect(screenX - screenX / 2.5f, screenY - screenY / 1.39f, 64, 64), !isPort, "", this.testGUIskin.customStyles[0]);

       GUI.Label(new Rect(100f, screenY - screenY / 1.75f, screenX / 5, screenY / 12), "Einführung starten", lblStyle);
       isTutorial = GUI.Toggle(new Rect(screenX - screenX / 2.5f, screenY - screenY / 1.7f, 64, 64), this.isTutorial, "", this.testGUIskin.customStyles[0]);

        
        PlayerPrefs.SetInt("isSound", System.Convert.ToInt16(isSound));
        PlayerPrefs.SetInt("isRotate", System.Convert.ToInt16(isTest));
        PlayerPrefs.SetInt("isPort", System.Convert.ToInt16(isPort));
        PlayerPrefs.SetInt("isTutorial", System.Convert.ToInt16(isTutorial));

        
/*
        GUI.Label(new Rect(100f, screenY - screenY / 1.76f, screenX / 5, screenY / 12), "Hochformat", lblStyle);
        isPort = GUI.Toggle(new Rect(screenX - screenX / 2.5f, screenY - screenY / 1.76f, 64, 64), this.isPort, "", this.testGUIskin.customStyles[0]);

        GUI.Label(new Rect(100f, screenY - screenY / 2.25f, screenX / 5, screenY / 12), "Querformat", lblStyle);
        isLand = GUI.Toggle(new Rect(screenX - screenX / 2.5f, screenY - screenY / 2.25f, 64, 64), !isPort, "", this.testGUIskin.customStyles[0]);

        PlayerPrefs.SetInt("isSound", System.Convert.ToInt16(isSound));
        PlayerPrefs.SetInt("isRotate", System.Convert.ToInt16(isRotate));
        PlayerPrefs.SetInt("isPort", System.Convert.ToInt16(isPort));
        */


        /*//print(System.Convert.ToBoolean(PlayerPrefs.GetInt("isSound")));
       print(System.Convert.ToBoolean(PlayerPrefs.GetInt("isRotate")));
       
       
      // toggleSound = GUI.Toggle(new Rect(screenX - screenX / 1.5f, screenY - screenY / 1.02f, screenX / 5, screenY / 12), toggleSound, "ON");
       
       
       if (GUI.Button(new Rect(screenX / 5, screenY - screenY / 12, screenX / 5, screenY / 12), "Info"))
       {
            
       }
       if (GUI.RepeatButton(new Rect(2 * screenX / 5, screenY - screenY / 12, screenX / 5, screenY / 12), "Locations"))
       {
           
       }



       //Update map and center user position 


       if (GUI.Button(new Rect(3 * screenX / 5, screenY - screenY / 12, screenX / 5, screenY / 12), "Restart"))
       {
            
       }

       //Show GPS Status info. Please make sure the GPS_Status.cs script is attached and enabled in the map object.
       if (GUI.Button(new Rect(4 * screenX / 5, screenY - screenY / 12, screenX / 5, screenY / 12), "Quit"))
       {


           Application.Quit();
       }
       //GUI.EndGroup ();


     */

        if (isSound)
        {
            optionImg = Resources.Load("sound") as Texture2D;
            AudioListener.pause = false;
        }
        else
        {
            optionImg = Resources.Load("mute") as Texture2D;
            AudioListener.pause = true;
        }
        if (GUI.Button(new Rect((Screen.width - optionImg.width) - 10f, 5f, optionImg.width, optionImg.height), "", new GUIStyle()))
        {
            isSound = !isSound;
            PlayerPrefs.SetInt("isSound", System.Convert.ToInt16(isSound));
        }
        GUI.DrawTexture(new Rect((Screen.width - optionImg.width) - 10f, 5f, optionImg.width, optionImg.height), optionImg);


    }

}
