using UnityEngine;
using System.Collections;

public class MoveByLinearAcc_Camera : MonoBehaviour
{
    public GameObject myCamera;
    private Vector3 offSet;
	// Use this for initialization
	void Start () {
        offSet = transform.position; 
		Sensor.Activate(Sensor.Type.Accelerometer);
	}
    
	
	void FixedUpdate () {
        Vector3 linearAcc = FilterMax(Sensor.accelerometer / 20 * 150 * Time.deltaTime);      
       // transform.Rotate( new Vector3(-linearAcc.x, rigidbody.position.y, -linearAcc.y));
	}
    void LateUpdate()
    {
        // transform.position = player.transform.position + offSet;
        Vector3 linearAcc = FilterMax(Sensor.accelerometer / 20 * 1 * Time.deltaTime);
       // transform.Rotate(new Vector3(-linearAcc.x, rigidbody.position.y, -linearAcc.y));
        print(linearAcc.ToString());
        transform.Rotate((new Vector3(-linearAcc.x, rigidbody.position.y, -linearAcc.y)) *Time.deltaTime, 0.2f);
       // rigidbody.AddForce(linearAcc*5f);
    }
	
	Vector3 holder = Vector3.zero;
	Vector3 max = Vector3.zero;
	Vector3 velocity = Vector3.zero;
	
	Vector3 FilterMax(Vector3 input)
	{
		if(input.magnitude > max.magnitude) max = input; 
		   
		holder = Vector3.SmoothDamp(holder, max, ref velocity, 0.1f);
		if(Vector3.Distance(holder, max) < 0.4f) max = Vector3.zero;
		return holder;
	}
}