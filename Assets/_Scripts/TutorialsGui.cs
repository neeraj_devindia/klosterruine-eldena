﻿using UnityEngine;
using System.Collections;
using System.IO;

public class TutorialsGui : MonoBehaviour
{
    public Texture2D box;
    public Texture2D trig1;
    public Texture2D trig2;
    private float height;
    private float width;
    private Font font;

    private float screenX;
    private float screenY;

    string[] textused = new string[5];
    private Texture2D[] sliderImgs = new Texture2D[5];
    int index;
    Vector2 scrollPosition;

    public bool isTutorial = true;

    void Awake()
    {
        screenX = Screen.width;
        screenY = Screen.height;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    public bool firsttime = true;
    bool checkedfirsttime = true;
    void checkforfirsttime()
    {
        FileInfo file = new FileInfo(Application.dataPath + "/appdata.txt");
        firsttime = true;
        checkedfirsttime = true;

        if (file.Exists)
        {
            firsttime = true;
            checkedfirsttime = true;
        }
        else
        {
            firsttime = false;
            checkedfirsttime = false;
        }
    }
    void Start()
    {
        isTutorial = System.Convert.ToBoolean(PlayerPrefs.GetInt("isTutorial"));
        checkforfirsttime();
        cm = gameObject.GetComponent<cameramovementrotation>();
        if (checkedfirsttime)
        {
            FileInfo file = new FileInfo(Application.dataPath + "/appdata.txt");
            file.Delete();
            isTutorial = true;
        }


        height = Screen.height / 8;
        width = Screen.width / 5;

        index = 0;
        scrollPosition = Vector2.zero;
        font = Resources.Load("FuturaBK_WindowText") as Font;
        //********************************************************************************************
        for (int i = 0; i < 5; i++)
        {
            sliderImgs[i] = idleImg;
        }

        textused[0] = @"Willkommen zur digitalen Führer zur Klosterruine Eldena.

Diese kurze Einführung wird sie mit den Funktionen der Applikation vertraut machen. Den besten Eindruck bekommen Sie wenn Sie vor Ort sind und sich an den markierten Stellen die rekonstruierten Panoramen anschauen.

Bitte wischen sie nach links um zur nächsten Seite zu gelangen.";

        textused[1] = @"MODELL

Hier können Sie sich das 3D Modell anschauen. Nutzen Sie die Wischgeste um zu rotieren und Kneifgesten/ Fingerspreizgesten um heran- oder herauszuzoomen.

Tippen Sie auf eine der Zahlen um in die interaktiven Panoramabilder zu gelangen.";
        textused[2] = @"ENTDECKEN

Mit dieser Funktion erhalten Sie per Bilderkennung zusätzliche Informationen and ausgewählten Stellen. Halten Sie ihr Gerät so, dass sie Folgendes durch die Kamera sehen:

1. Grundplan auf den Schildern

2. Punkte 1-5 um in das jeweilige virtuelle Panorama zu gelangen.

3. Grabplatten

Mit dem Knopf am oberen Bildschirmende schalten Sie zwischen der Ruine und dem Klostermodell um.";
        textused[3] = @"GESCHICHTE

Hier finden Sie interessante Informationen und Bilder zur Klosterstätte.

Wischen Sie horizontal, um umzublättern.";
        textused[4] = @"EINSTELLUNGEN

In diesem Menü können sie zwischen Hoch- und Querformat wechseln.

Nach dem ersten Start der Applikation wird diese Einleitung nicht mehr angezeigt. Sie können sie jedoch jederzeit unter diesem Menü wieder aufrufen.

Wir wünschen Ihnen viel Spass dabei, das rekonstruierte Kloster zu erforschen.
";

    }


    Vector2 dpos = Vector2.zero;
    Vector2 dpos1 = Vector2.zero;
    bool haveLastTouch = false;
    float labelposition;

    void Update()
    {
        if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
        {
            dpos = Input.touches[0].position;

        }

        if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Ended)
        {
            dpos1 = Input.touches[0].position;
            if ((dpos.x - dpos1.x) > 200)
            {
                index = index - 1;
                if (index < 0)
                {
                    index = 4;
                }
                labelposition = Screen.height * 0.06f;
            }
            if ((dpos.x - dpos1.x) < -200)
            {
                index = index + 1;
                if (index > 4)
                {
                    index = 0;
                }
                labelposition = Screen.height * 0.06f;
            }

        }


        if (Input.touchCount > 0)
        {

            labelposition -= (Input.touches[0].deltaPosition.y * 4f);
            if (labelposition > Screen.height * 0.06f)
            {
                labelposition = Screen.height * 0.06f;
            }
            if (labelposition < -2500)
            {
                labelposition = -2500;
            }

        }
        //********************************************************************************************
        for (int i = 0; i < 5; i++)
        {
            sliderImgs[i] = idleImg;
            if (i == index)
            {
                sliderImgs[i] = activeImg;
            }
        }
    }

    private GUIStyle buttonstyle;
    private GUIStyle labelstyle;

    Vector2 move;
    int labelfont;
    public Texture2D blackback;
    public Texture2D leftarrow;
    public Texture2D rightarrow;
    public Texture2D close;

    public Texture2D idleImg;
    public Texture2D activeImg;
    cameramovementrotation cm;
    void OnGUI()
    {
        screenX = Screen.width;
        screenY = Screen.height;

        if (Screen.height < Screen.width)
        {
            labelfont = (int)(Screen.height * 0.05f);
        }
        else
        {
            labelfont = (int)(Screen.width * 0.05f);
        }

        if (Screen.width >= Screen.height)
        {
            GUI.skin.button.fontSize = (int)Mathf.Round(12 * Screen.width / 480);
            GUI.skin.box.fontSize = (int)Mathf.Round(12 * Screen.width / 320);
        }
        else
        {
            GUI.skin.button.fontSize = (int)Mathf.Round(12 * Screen.height / 480);
            GUI.skin.box.fontSize = (int)Mathf.Round(12 * Screen.height / 320);
        }

        buttonstyle = GUI.skin.GetStyle("Button");
        buttonstyle.normal.background = Resources.Load("btn3") as Texture2D;
        buttonstyle.hover.background = Resources.Load("btn3") as Texture2D;
        buttonstyle.normal.textColor = Color.white;
        labelstyle = GUI.skin.GetStyle("Label");

        labelstyle.font = font;
        //buttonstyle.font = font;
        //buttonstyle.fontSize = (int)((Screen.width + Screen.height) * 0.026f);

        //labelstyle.fontSize = (int)(Screen.width * 0.02f);
        buttonstyle.alignment = TextAnchor.LowerCenter;
        buttonstyle.imagePosition = ImagePosition.ImageAbove;

        if (isTutorial)
        {
            if (cm.enabled)
            {
                cm.enabled = false;
            }
            GUI.DrawTexture(new Rect(screenX / 8f, screenY - screenY / 1.1f, screenX - (screenX / 8f + screenX / 8f), (screenY - (screenY / 8) * 2) - (screenY - screenY / 1.1f)), box);

            if (index == 1)
                GUI.DrawTexture(new Rect(screenX / 8f, screenY - screenY / 4, screenX / 8f, screenY / 8), trig1);

            if (index == 2)
                GUI.DrawTexture(new Rect(screenX / 4f + screenX / 8f, screenY - screenY / 4, screenX / 8f, screenY / 8), trig1);

            if (index == 3)
                GUI.DrawTexture(new Rect(2 * screenX / 4f, screenY - screenY / 4, screenX / 8f, screenY / 8), trig2);

            if (index == 4)
                GUI.DrawTexture(new Rect(3 * screenX / 4f, screenY - screenY / 4, screenX / 8f, screenY / 8), trig2);


            if (Screen.height > Screen.width)
            {
                if (GUI.Button(new Rect(3.2f * screenX / 4f, screenY - screenY / 1.11f, Screen.width * 0.06f, Screen.height * 0.06f), close, new GUIStyle()))
                {
                    isTutorial = false;
                    cm.enabled = true;
                }
                if (GUI.Button(new Rect(screenX / 10.5f, (Screen.height - (Screen.height * 0.01f)) / 4f, Screen.width * 0.1f, Screen.height * 0.1f), leftarrow, new GUIStyle()))
                {
                    index = index - 1;
                    if (index < 0)
                    {
                        index = 4;
                    }
                    labelposition = 0;

                }
                if (GUI.Button(new Rect((screenX / 10f) + (screenX - (screenX / 8f + screenX / 8f)), (Screen.height - (Screen.height * 0.01f)) / 4f, Screen.width * 0.1f, Screen.height * 0.1f), rightarrow, new GUIStyle()))
                {
                    index = index + 1;
                    if (index == 5)
                    {
                        index = 0;
                    }
                    labelposition = 0;

                }
            }
            else
            {
                if (GUI.Button(new Rect(3.35f * screenX / 4f, screenY - screenY / 1.13f, Screen.width * 0.06f, Screen.height * 0.06f), close, new GUIStyle()))
                {
                    isTutorial = false;
                    cm.enabled = true;
                }
                if (GUI.Button(new Rect(screenX / 8.9f, (Screen.height - (Screen.height * 0.01f)) / 4f, Screen.width * 0.1f, Screen.height * 0.1f), leftarrow, new GUIStyle()))
                {
                    index = index - 1;
                    if (index < 0)
                    {
                        index = 4;
                    }
                    labelposition = 0;

                }
                if (GUI.Button(new Rect((screenX / 8.9f) + (screenX - (screenX / 8f + screenX / 8f)), (Screen.height - (Screen.height * 0.01f)) / 4f, Screen.width * 0.1f, Screen.height * 0.1f), rightarrow, new GUIStyle()))
                {
                    index = index + 1;
                    if (index == 5)
                    {
                        index = 0;
                    }
                    labelposition = 0;
                }
            }

            scrollPosition = GUI.BeginScrollView(new Rect(screenX / 8f, screenY - screenY / 1.2f, screenX - (screenX / 8f + screenX / 8f), (screenY - (screenY / 8) * 2) - (screenY - screenY / 1.3f) - labelposition), new Vector2(200f, labelposition), new Rect(0, 0f, Screen.width * 0.7f, 0), false, false);
            print(labelposition + (screenY - screenY / 1.01f).ToString());
            GUIStyle style = new GUIStyle();
            style.richText = true;
            style.font = font;
            style.fontSize = (int)(Screen.width * 0.05f);
            style.normal.textColor = Color.black;
            style.wordWrap = true;
            style.padding = new RectOffset(0, 10, 0, 10);
            style.alignment = TextAnchor.MiddleLeft;

            GUIStyle guiSt = new GUIStyle();
            guiSt.font = font;
            guiSt.fontSize = (int)(Screen.width * 0.05f);
            guiSt.normal.textColor = Color.black;
            guiSt.wordWrap = true;
            guiSt.fontSize = labelfont;

            GUI.Label(new Rect(30, labelposition + (screenY - screenY / 1.01f), screenX - (screenX / 8f + screenX / 8f) - 50, (screenY - (screenY / 8) * 2) - (screenY - screenY / 1.1f)), textused[index], guiSt);
            GUI.EndScrollView();

            if (GUI.Button(new Rect(Screen.width * 0.4f, screenY - screenY / 3.3f, 23, 23), sliderImgs[0], new GUIStyle()))
            {
                index = 0;
            }
            if (GUI.Button(new Rect(Screen.width * 0.45f, screenY - screenY / 3.3f, 23, 23), sliderImgs[1], new GUIStyle()))
            {
                index = 1;
            }
            if (GUI.Button(new Rect(Screen.width * 0.5f, screenY - screenY / 3.3f, 23, 23), sliderImgs[2], new GUIStyle()))
            {
                index = 2;
            }
            if (GUI.Button(new Rect(Screen.width * 0.55f, screenY - screenY / 3.3f, 23, 23), sliderImgs[3], new GUIStyle()))
            {
                index = 3;
            }
            if (GUI.Button(new Rect(Screen.width * 0.6f, screenY - screenY / 3.3f, 23, 23), sliderImgs[4], new GUIStyle()))
            {
                index = 4;
            }
        }
    }
}
