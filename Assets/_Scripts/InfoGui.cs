﻿using UnityEngine;
using System.Collections;

public class InfoGui : MonoBehaviour
{


    Texture2D[] images = new Texture2D[6];
    int index;
    Vector2 scrollPosition;
    Font font;
    string[] textused = new string[6];

    private bool isSound = true;
    public Texture2D optionImg;
    public bool isPortrait = true;
    public Texture2D idleImg;
    public Texture2D activeImg;
    private Texture2D[] sliderImgs = new Texture2D[6];

    void Start()
    {
        labelposition = Screen.height * 0.06f;
        isPortrait = System.Convert.ToBoolean(PlayerPrefs.GetInt("isPort"));

        if (isPortrait)
            Screen.orientation = ScreenOrientation.Portrait;
        else
            Screen.orientation = ScreenOrientation.Landscape;
        optionImg = Resources.Load("rotate-icon") as Texture2D;
        isSound = System.Convert.ToBoolean(PlayerPrefs.GetInt("isSound"));

        images[0] = Resources.Load("Info Images/DSC07467") as Texture2D;
        images[1] = Resources.Load("Info Images/DSC07478") as Texture2D;
        images[2] = Resources.Load("Info Images/DSC07485") as Texture2D;
        images[3] = Resources.Load("Info Images/DSC07491") as Texture2D;
        images[4] = Resources.Load("Info Images/DSC07495") as Texture2D;
        images[5] = Resources.Load("Info Images/DSC07500") as Texture2D;

        textused[0] = @"<color=red>Wege zum Kloster</color>

Die Klosterruine Eldena liegt etwa 5 km von der Altstadt entfernt im Osten der Universität- und Hansestadt Greifswald nahe der Mündung des Ryck in die Dänische Wiek. Man erreicht sie auf dem Weg entlang des Ryck zum ehemaligen Fischerdorf Wieck bequem zu Fuß oder mit dem Fahrrad. Mit Bus oder Auto geht es in Richtung Lubmin und Wolgast zum Ortsteil Eldena. Auch eine etwa halbstündige Schiffspartie mit der „Stubnitz"" vom Altstadthafen bis zur Anlegestelle in Wieck lohnt sich. Über den „Studentensteig"" ist Eldena von dort in wenigen Minuten zu Fuß zu erreichen. Segler können auch über den Greifswalder Bodden im Wiecker Hafen einlaufen, um von hier aus die Klosterruine als einen ganz besonderen Ort unter den Klosterstätten in Mecklenburg-Vorpommern zu besuchen. Durch den in Greifswald geborenen Maler Caspar David Friedrich ist die Ruine des ehemaligen Zisterzienserklosters zu einem Sinnbild der romantischen Malerei geworden. Zu seinen Lebzeiten befanden sich das Klostergelände und die Ländereien im Besitz der Universität, die das Amt Eldena vom letzten Pommernherzog Bogislaw XIV. im Jahr 1634 erhalten hatte. Aus der nachreformatorischen Geschichte des Klosters ist der Ausbau des Amtsgutes und die Gründung eine r Landwirtschaftsakademie 1835 hervorzuheben, die bis 1876 bestand. Die noch vorhandenen Klostergebäude wurden ebenfalls landwirtschaftlich genutzt. Seit 1937 im Besitz der Stadt Greifswald, ist die Klosterruine im Sommer Theaterspielort und bietet alljährlich am ersten Juliwochenende die Kulisse für die Eldenaer Jazz Evenins. Die Klosterruine ist Station auf dem Caspar-David- Friedrich-Bildweg und der Europäischen Route der Backsteingotik.";


        textused[1] = @"<color=red>Die Klosterruine als Symbol der Romantik</color>

Mit seinen Gemälden und Zeichnungen machte Caspar David Friedrich (1774- 1840) die Ruine des mittelalterlichen Zisterzienserklosters Eldena weithin bekannt. Die Überreste des einst bedeutenden Klosters sind eingebettet in eine Parkanlage mit altem Baumbestand , darunter 180jährige Eichen, welche die Ideen Friedrichs verbildlichen und die romantische Atmosphäre der Anlage prägen. Den Hauptakzent der Ruine bildet die imposante Westfassade der ehemaligen Klosterkirche mit der hohen spitzbogigen Fensteröffnung.
Die Rettung der Überreste der mittelalterlichen Klosteranlage, die nach Plünderungen im Dreißigjährigen Krieg durch kaiserliche und schwedische Truppen mehr und mehr verfallen war und schließlich seit der zweiten	Hälfte des 17. Jh.s als Steinbruch für Festungs- und Greifswalder Universitätsbauten genutzt wurde,  ist wesentlich dem Eingreifen des von romantischen Ideen begeisterter preußischen Kronprinzen Friedrich Wilhelm (1795-1861, späterer König Friedrich Wilhelm IV.) zu verdanken, der die Ruine 1827 in einem verwahrlosten Zustand vor gefunden hatte. Hierauf erfolgten von 1828 bis 1832 erste Aufräumungs- und Sanierungsarbeiten sowie die Anlegung eines Parks zur Erschließung des Geländes.
Anstelle der fehlenden Langhauspfeiler des Kirchenschiffs wurden Eichen gesetzt. Das Verschmelzen der ewig wiederkehrenden Natur mit den ehr würdigen baulichen Zeugnissen einer unwiederbringlich vergangenen Zeit trägt eine tiefgründige Symbolikin sich, die auch den heutigen Besucher zu allen Jahres- und Tageszeiten in ihren Bann zieht.";


        textused[2] = @"<color=red>Die Klostergründung</color>

In der Zeit dänischer Expansion in den südlichen Ostseeraum waren in den 1170er Jahren vom dänischen Zisterzienserkloster Esrom auf Seeland aus Tochtergründungen in Dargun und
Kolbatz erfolgt. Kriegerische Auseinandersetzungen im umstrittenen Grenzgebiet zwangen die Darguner Zisterzienser Ende des 12. jhs. zur Aufgabe ihres Klosters.	Der Überlieferung nach gründeten die v ertriebenen Mönc he 1199 an der Mündung des Ryck unter dem Schutz des Rügenfürsten jaromar 1. ein neues Kloster.
In dieser Gegend waren den Darguner Zisterziensern schon vorher Salzpfannen überlassen worden. Der Zufluchtsort der Mönche sollte zur Keimzelle des neuen Klosters „Hilda"" (Eldena) und der späteren Stadt Greifswald werden.  1204 wurde das Kloster ""Hilda"" von Papst Innozenz III. bestätigt. Vom Rügenfürsten wurde das Kloster mit umfangreichem Landbesitz und mit Privilegien ausgestattet. In einer  Bestätigungsurkunde von  1209 wurde dem Kloster das Recht verliehen, Siedler - Deutsche, Wenden und Dänen - ins Land zu holen. Im Zuge der deutschen Ostkolonisation kamen Bauern und Handwerker, welche neue Dörfer anlegten und die Kulturlandschaf t der Region nachhaltig prägten. Das Kloster mit seinem reichen Grundbesitz war geistliches und wirtschaftliches Zentrum des Gebietes. Unter den in einer Urkunde von 1248 aufgeführten Besitzungen wird das „oppidum Gripheswald"" erstmals urkundlich erwähnt, dem 1250 durch Herzog Wartislaw III. von Pommern-Demmin das Stadtrecht verliehen wurde. Zu den Privilegien gehörte der Fischfang in einem Teil des Greifswalder Boddens und der Betrieb von Mühlen und Krügen. Die Lage in Hafennähe an der Dänischen Wiek sicherte Handelsverbindungen auch zu Wasser.";


        textused[3] = @"<color=red>Zur Baugeschichte des Klosters</color> 

Die ältesten, noch romanischen Teile der Klosterkirche stammen aus der ersten Hälfte des 13. jhs. Erhalten sind die wohl um 1210/15 errichtete Chorsüdwand, die als älteste Backsteinmauer des vorpommerschen Festlandes gilt, Teile der quadratischen Vierung und des anschließenden östlichen Langhausjochs sowie Teile des Querschiffes, im südlichen Querschiffsarm  mit Gewölbeansätzen. Charakteristisch für die älteren Bauteile ist eine kräftige Profilierung von Pfeilern und Arkaden mit zu Gruppen zusammengefassten  Halbsäulenvorlagen. Diese östlichen Partien der Klostterkirche waren um die Mitte des 13. Jh.s fertiggestellt. Der ursprünglich gerade geschlossene Chor wurde später durch ein Polygon, dessen Grundriss ergraben werden konnte, nach Osten erweitert.
 
Der erhaltene östliche Klausurflügel, der u.a. die Sakristei, den Kapitelsaal, die sogenannte „Abtskapelle"" und im Obergeschoss das Dormitorium (den Schlafsaal der Mönche) beherbergte, wurde um 1260 errichtet. Von der Qualität der Architekturplastik, der Kapitelle und Säulenbasen aus den Klausurgebäuden kann man sich heute im Pommerschen Landesmuseum überzeugen.
Die Westfassade mit dem maßwerkgeschmückten Treppenturm und die Achteckpfeiler des Langhauses wurden um 1400, der Blütezeit des Klosters, aufgeführt. Ebenfalls aus mittelalterlicher Zeit stammen der Überrest der südlichen Klostermauer und das letzte erhaltene Wirtschaftsgebäude des Klosters, die sogenannte ""Klosterscheune"", die gegenwärtig zu einem vielseitig nutzbaren Informationszentrum ausgebaut wird.";
        textused[4] = @"<color=red>Die Klosterkirche als herzogliche Grablege</color>
		 	
			Das Kloster Eldena spielte im Mittelalter auch als Grablege der pommerschen Herzöge eine bedeutende Rolle. Aus dem pommerschen Herzogshaus stammt auch Wartislaw III., dem Greifswald die Verleihung des Stadtrechts von 1250 verdankt. Auf urkundlich verbürgte herzogliche Bestattungen in der Klosterkirche bis ins beginnende 16. Jh. weist die Gedenktafel an der Chorsüdwand hin.
				Erhalten ist von den sicher reich ausgestatteten herzoglichen Grabmälern leider nichts. Die seit 1843 an den Wänden des südlichen Querschiffes und der Klausur eingemauerten Grabplatten von Äbten und Adligen waren zum überwiegenden Teil erst im 19. jh. wieder aus verschiedenen Orten der Umgebung zusammengetragen und im Vierungsbereich  vorerst liegend angeordnet worden. Mittelalterliche Grabplatten von Eldenaer Äbten befinden sich auch im Greifswalder Dom St. Nikolai, im Hauptgebäude der Ernst-Moritz­ Arndt-Universität und im Pommerschen Landesmuseum.
				
				Weitere Klosterstätten in Vorpommern und Ostmecklenburg";
        textused[5] = @"<color=red>Krummin auf Usedom,Kirche St.Michael</color>

Der heutige, gotische Backsteinbau ist die ehemalige Klosterkirche des um 1302 gegründeten Z istersienserinnenklosters Krummin.
Bergen auf  Rügen, Marienkirche
Kirche des 1193 mit Nonnen aus Roskilde besetzten Klosters, ältester Backsteinbau Rügens, bedeutender Bestand romanischer Wandmalerei
Dargun, Kloster- und Schlossanlage
Zister ziense rkloster, Gründung des dänischen Klosters Esrom, Ende
12. J h. zerstör t, Neugründung 1209 durch das Kloster Doberan,
16. J h. Umbau zum  herzoglichen Schloss, 1945 ausgebrannt, seit 1990 umfangreiche Sicherungsmaßnahmen,	Brauhaus,	Sa longebäude,  Museum
Franzburg, Kirche des eherm.Zisterzienserklosters Neuenkamp 1231 Gründung durch den Rügenf ürsten Witzlaw 1., erhalten ist das südliche Querschiff; Klostergarten, Rathaus 18. Jh.
Ribnitz-Damgarten,Deutsches  Bernsteinmuseum
Ehemaliges Klarissenkloster, gestiftet 1323/24, neben Rühn, Dobbertin und Malchow nach der Reformation Landeskloster, Klosterkircher
heute Museum, Dauerausstellung ,,Dame vo n Welt - aber auc h Nonne"", Stiftsgebäude";


        index = 0;
        scrollPosition = Vector2.zero;
        font = Resources.Load("FuturaBK_WindowText") as Font;

        //********************************************************************************************
        for (int i = 0; i < 6; i++)
        {
            sliderImgs[i] = idleImg;
        }
    }
    Vector2 dpos = Vector2.zero;
    Vector2 dpos1 = Vector2.zero;
    bool haveLastTouch = false;

    // Update is called once per frame
    float labelposition;
    void Update()
    {
        if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
        {
            dpos = Input.touches[0].position;

        }

        if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Ended)
        {
            dpos1 = Input.touches[0].position;
            if ((dpos.x - dpos1.x) > 200)
            {
                index = (index - 1) % images.Length;
                if (index < 0)
                {
                    index = 5;
                }
                labelposition = Screen.height * 0.06f;
            }
            if ((dpos.x - dpos1.x) < -200)
            {
                index = (index + 1) % images.Length;
                labelposition = Screen.height * 0.06f;
            }

        }


        if (Input.touchCount > 0)
        {

            labelposition -= (Input.touches[0].deltaPosition.y * 4f);
            if (labelposition > Screen.height * 0.06f)
            {
                labelposition = Screen.height * 0.06f;
            }
            if (labelposition < -2500)
            {
                labelposition = -2500;
            }

        }
        isPortrait = System.Convert.ToBoolean(PlayerPrefs.GetInt("isPort"));

        if (isPortrait)
            Screen.orientation = ScreenOrientation.Portrait;
        else
            Screen.orientation = ScreenOrientation.Landscape;


        //********************************************************************************************
        for (int i = 0; i < 6; i++)
        {
            sliderImgs[i] = idleImg;
            if (i == index)
            {
                sliderImgs[i] = activeImg;
            }
        }
    }
    Vector2 move;
    int labelfont;
    GUIStyle labelstyle;
    GUIStyle buttonstyle;
    public Texture2D blackback;
    public Texture2D leftarrow;
    public Texture2D rightarrow;

    public Texture2D slideBg;
    public Texture2D close;

    void OnGUI()
    {
        labelstyle = GUI.skin.GetStyle("Label");
        labelstyle = new GUIStyle();
        labelstyle.font = font;
        if (Screen.height < Screen.width)
        {
            labelfont = (int)(Screen.height * 0.05f);
        }
        else
        {
            labelfont = (int)(Screen.width * 0.05f);
        }
        labelstyle.fontSize = labelfont;

        buttonstyle = GUI.skin.GetStyle("Button");
        buttonstyle.alignment = TextAnchor.MiddleCenter;
        GUI.skin.verticalScrollbarThumb.fixedWidth = Screen.width / 20;
        GUI.skin.verticalScrollbar.fixedWidth = Screen.width / 20;
        GUI.DrawTexture(new Rect(0, labelposition, Screen.width, (Screen.height - (Screen.height * 0.25f)) / 2), images[index]);

        if (GUI.Button(new Rect(20, (Screen.height - (Screen.height * 0.3f)) / 4f, Screen.width * 0.1f, Screen.height * 0.1f), leftarrow, new GUIStyle()))
        {
            index = (index - 1) % images.Length;
            if (index < 0)
            {
                index = 5;
            }
            labelposition = 0;

        }
        if (GUI.Button(new Rect(Screen.width * 0.9f, (Screen.height - (Screen.height * 0.3f)) / 4f, Screen.width * 0.1f, Screen.height * 0.1f), rightarrow, new GUIStyle()))
        {
            index = (index + 1) % images.Length;
            labelposition = Screen.height * 0.06f;
        }

        scrollPosition = GUI.BeginScrollView(new Rect(10, labelposition + ((Screen.height - (Screen.height * 0.12f)) / 2), Screen.width, Screen.height / 2.4f - labelposition), new Vector2(labelposition, labelposition), new Rect(0, 0f, Screen.width * 0.9f, 0));

        GUIStyle style = new GUIStyle();
        style.richText = true;
        style.font = font;
        style.fontSize = (int)(Screen.width * 0.05f);
        style.normal.textColor = Color.black;
        style.wordWrap = true;
        style.padding = new RectOffset(0, 10, 0, 10);
        style.alignment = TextAnchor.MiddleLeft;

        GUIStyle guiSt = new GUIStyle();
        guiSt.font = font;
        guiSt.fontSize = (int)(Screen.width * 0.05f);
        guiSt.normal.textColor = Color.black;
        guiSt.wordWrap = true;
        guiSt.fontSize = labelfont;

        GUI.Label(new Rect(0, 0, Screen.width * 0.9f, 4f * Screen.height), textused[index], guiSt);
        GUI.EndScrollView();

        //______________________________________________________________________________

        if (isSound)
        {
            optionImg = Resources.Load("sound") as Texture2D;
            AudioListener.pause = false;
        }
        else
        {
            optionImg = Resources.Load("mute") as Texture2D;
            AudioListener.pause = true;
        }
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height * 0.06f), slideBg, ScaleMode.StretchToFill);
        if (GUI.Button(new Rect((Screen.width - optionImg.width) - 10f, 5f, optionImg.width, optionImg.height), "", new GUIStyle()))
        {
            isSound = !isSound;
            PlayerPrefs.SetInt("isSound", System.Convert.ToInt16(isSound));
        }
        GUI.DrawTexture(new Rect((Screen.width - optionImg.width) - 10f, 5f, optionImg.width, optionImg.height), optionImg);

        //__________________________________________________________________________________________________________________________________________________________________________

        //GUI.DrawTexture(new Rect(Screen.width * 0.36f, (Screen.height - (Screen.height * 0.29f)) / 2, Screen.width * 0.35f, Screen.height*0.06f), slideBg, ScaleMode.StretchToFill);


        if (GUI.Button(new Rect(Screen.width * 0.4f, Screen.height * 0.02f, activeImg.width, activeImg.height), sliderImgs[0], new GUIStyle()))
        {
            index = 0;
        }
        if (GUI.Button(new Rect(Screen.width * 0.45f, Screen.height * 0.02f, activeImg.width, activeImg.height), sliderImgs[1], new GUIStyle()))
        {
            index = 1;
        }
        if (GUI.Button(new Rect(Screen.width * 0.5f, Screen.height * 0.02f, activeImg.width, activeImg.height), sliderImgs[2], new GUIStyle()))
        {
            index = 2;
        }
        if (GUI.Button(new Rect(Screen.width * 0.55f, Screen.height * 0.02f, activeImg.width, activeImg.height), sliderImgs[3], new GUIStyle()))
        {
            index = 3;
        }
        if (GUI.Button(new Rect(Screen.width * 0.6f, Screen.height * 0.02f, activeImg.width, activeImg.height), sliderImgs[4], new GUIStyle()))
        {
            index = 4;
        }
        if (GUI.Button(new Rect(Screen.width * 0.65f, Screen.height * 0.02f, activeImg.width, activeImg.height), sliderImgs[5], new GUIStyle()))
        {
            index = 5;
        }

    }

}
