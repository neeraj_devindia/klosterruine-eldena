﻿using UnityEngine;
using System.Collections;

public class rotates : MonoBehaviour
{
    public float angle;
   

    void Update()
    {
        
        if (Input.touches.Length > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            if (Input.touchCount == 1)
            {
                angle -= Input.GetTouch(0).deltaPosition.y;
                transform.rotation = Quaternion.AngleAxis(angle, Vector3.up );
            }
        }
        
    }
}