﻿using UnityEngine;
using System.Collections;

public class Numberfacingcamera : MonoBehaviour {

	// Use this for initialization
    Quaternion rot;
	void Start () 
    {
        rot = mainrotationobject.transform.rotation;
	}
	public GameObject mainrotationobject;
	// Update is called once per frame
	void Update () 
    {
        mainrotationobject.transform.rotation = new Quaternion(rot.x, mainrotationobject.transform.rotation.y,rot.z, mainrotationobject.transform.rotation.w);
        transform.rotation = new Quaternion(mainrotationobject.transform.rotation.x,mainrotationobject.transform.rotation.y,mainrotationobject.transform.rotation.z,mainrotationobject.transform.rotation.w);
    }
}
