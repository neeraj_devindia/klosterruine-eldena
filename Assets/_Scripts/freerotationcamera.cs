﻿using UnityEngine;
using System.Collections;

public class freerotationcamera : MonoBehaviour {

	// Use this for initialization
	void Start () 
    {
        done = false;
        starttime = 0f;
		startmoving = 0f;
	}

	float starttime ;
	float startmoving;
	float speed=55f;
	bool comedown=false;
	bool zoomin = true;
	bool shouldrotate=false;
	float camerarotation=0f;
	bool comeup;
    public bool done;
	public GameObject target;
   
	// Update is called once per frame
	void Update () 
    {
		startmoving += Time.deltaTime;
		if (startmoving < 2.7f && zoomin==true)
		{
				camera.fieldOfView -= 0.7f;	
		} 
		else if(zoomin==true)
		{
		    zoomin=false;
			camerarotation=transform.eulerAngles.x-1;

		    comedown=true;
		}

		if (comedown==true) 
		{
		    transform.RotateAround (target.transform.position, -transform.right, speed * Time.deltaTime);
		    if(transform.eulerAngles.x<=19)
		    {

			    comedown=false;
			    shouldrotate=true;
		    }
		}
	      if(shouldrotate==true)
		  {
			starttime += Time.deltaTime;
	     	if (starttime < 3f)
		 	{
				transform.RotateAround (target.transform.position, -target.transform.up, speed * Time.deltaTime);
		    }
			else
			{
				comeup=true;
			}
		 }
		if (comeup == true&&done==false) 
		{
			transform.RotateAround(target.transform.position, transform.right, speed * Time.deltaTime);
			if(transform.eulerAngles.x>=(camerarotation))
			{
			
				done=true;
				comeup=false;
                GameObject.Find("Main Camera").AddComponent<Transitions>();
			}

		}

	}
}
