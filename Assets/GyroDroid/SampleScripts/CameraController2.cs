﻿using UnityEngine;
using System.Collections;

public class CameraController2 : MonoBehaviour
{
    public GameObject player;
    private Vector3 offSet;

    public GUIStyle guiSt;
    void Start()
    {
        offSet = transform.position; //assign initial position 
    }

    void LateUpdate()
    {
        transform.position = player.transform.position + offSet;
      
    }
    void OnGUI()
    {
              
        if (GUI.Button(new Rect( 5, 60, 300, 150), "EXIT GAME",guiSt))
        {
            Application.Quit();            
        }

    }
}