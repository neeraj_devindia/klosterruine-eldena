﻿using UnityEngine;
using System.Collections;

public class Transitions : MonoBehaviour {

	// Use this for initialization
    private float height;
    private float width;
	void Start ()
    {
        text = "Ruine";
        height = Screen.height / 8;
        width = Screen.width / 5;
        isRuined = false;
        layer1 = GameObject.FindGameObjectWithTag("layer1");
        fading fadingscprit = layer1.GetComponent<fading>();
        foreach (Material item in fadingscprit.mat2)
        {
            item.shader = Shader.Find("Bumped Diffuse");
        }
        Destroy(fadingscprit);       
        layer2 = GameObject.FindGameObjectWithTag("layer2");
        layer3 = GameObject.FindGameObjectWithTag("layer3");
        layer3.SetActive(false);
        layer1.SetActive(false);


	}
	
	// Update is called once per frame
  public  GameObject layer1;
  public  GameObject layer2;
  public GameObject layer3;
    private float screenX;
    private float screenY;

    void Awake()
    {
        screenX = Screen.width;
        screenY = Screen.height;
    }
	void Update () 
    {
	
	}
      private GUIStyle buttonstyle;
      string text;
      bool isRuined;
      float buttonwidth;
        void OnGUI()
    {
        screenX = Screen.width;
        screenY = Screen.height;

        if (Screen.width >= Screen.height)
        {
            GUI.skin.button.fontSize = (int)Mathf.Round(12 * Screen.width / 480);
            GUI.skin.box.fontSize = (int)Mathf.Round(12 * Screen.width / 320);
            buttonwidth = Screen.width / 5f;
        }
        else
        {
            GUI.skin.button.fontSize = (int)Mathf.Round(12 * Screen.height / 480);
            GUI.skin.box.fontSize = (int)Mathf.Round(12 * Screen.height / 320);
            buttonwidth = Screen.height / 5f;
        }

        buttonstyle = GUI.skin.GetStyle("Button");
        buttonstyle.normal.background = Resources.Load("btn3") as Texture2D;
        buttonstyle.hover.background = Resources.Load("btn3") as Texture2D;
        buttonstyle.normal.textColor = Color.white;
      
     
        //buttonstyle.fontSize = (int)((Screen.width + Screen.height) * 0.026f);

        //labelstyle.fontSize = (int)(Screen.width * 0.02f);
        buttonstyle.alignment = TextAnchor.MiddleCenter;
      


        //GUI.skin.button.normal.background = (Texture2D)content.image;
        //GUI.skin.button.hover.background = (Texture2D)content.image;
        //GUI.skin.button.active.background = (Texture2D)content.image;

        
        if (GUI.Button(new Rect(0, 0, buttonwidth, screenY / 12), new GUIContent(text), buttonstyle))
        {
            if (text=="Ruine")
            {
                text = "Kloster Modell";
                layer1.SetActive(true);
                layer2.SetActive(false);
            }
            else
            {
                text = "Ruine";
                layer2.SetActive(true);
                layer1.SetActive(false);
            }
        }
       

    }
   
}
