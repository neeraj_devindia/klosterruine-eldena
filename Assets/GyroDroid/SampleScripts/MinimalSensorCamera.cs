// prefrontal cortex -- http://prefrontalcortex.de
// Full Android Sensor Access for Unity3D
// Contact:
// 		contact@prefrontalcortex.de

using UnityEngine;
using System.Collections;

public class MinimalSensorCamera : MonoBehaviour
{
    // Use this for initialization
    GameObject pointer;
    Camera[] cm;
    public static bool isRotate = false;
    public static bool isInitialize = true;
   
    void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
        SensorHelper.ActivateRotation();
        useGUILayout = false;
        //cm = Camera.allCameras;
        //pointer = GameObject.Find("3D_Pointer");
    }

    void Update()
    {
      transform.rotation = SensorHelper.rotation;      
    }    
    void FixUpdate()
    {


    }
}