﻿using UnityEngine;
using System.Collections;

public class Link_Click : MonoBehaviour
{
    private float height;
    private float width;
    bool displayDialog = false;
    // Use this for initialization

    public GameObject wind;
    Font font;

    private float screenX;
    private float screenY;

    void Awake()
    {
        screenX = Screen.width;
        screenY = Screen.height;
    }

    void Start()
    {
        font = Resources.Load("FuturaBK_WindowText") as Font;
        height = Screen.height / 10;
        width = Screen.width / 4;

        wind.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        height = Screen.height / 10;
        width = Screen.width / 4;

    }
    string content = "In the 12th century the Baltic coast south of the island of Rügen belonged to the Rani principality of Rügen, which in its turn was subject to the Danes. ";
    void OnMouseUp()
    {

        wind.SetActive(true);
        string nm = gameObject.name;
        print(nm);
        displayDialog = true;
    }
    GUIStyle labelstyle;
    GUIStyle buttonstyle;
    public Texture2D closeImg;
    void OnGUI()
    {
        screenX = Screen.width;
        screenY = Screen.height;

        if (Screen.width >= Screen.height)
        {
            GUI.skin.button.fontSize = (int)Mathf.Round(12 * Screen.width / 480);
            GUI.skin.box.fontSize = (int)Mathf.Round(12 * Screen.width / 320);
        }
        else
        {
            GUI.skin.button.fontSize = (int)Mathf.Round(12 * Screen.height / 480);
            GUI.skin.box.fontSize = (int)Mathf.Round(12 * Screen.height / 320);
        }
        labelstyle = GUI.skin.GetStyle("Label");
        buttonstyle = GUI.skin.GetStyle("Button");

        if (displayDialog)
        {
            labelstyle.fontSize = (int)(Screen.width * 0.05f);
            buttonstyle.fontSize = (int)(Screen.width * 0.02f);
            labelstyle.font = font;
            labelstyle.normal.textColor = Color.white;
            GUI.Label(new Rect(((0.125f * width) + (width * 0.02f)), screenY - screenY / 1.18f, (4 * width) - (width * 0.33f), 7 * height), content, labelstyle);
            if (GUI.Button(new Rect((Screen.width - (width * 0.38f)), screenY - screenY / 1.18f, closeImg.width + 5f, closeImg.height + 5f), closeImg, new GUIStyle()))
            {
                displayDialog = false;
                wind.SetActive(false);
            }
            //if (GUI.Button(new Rect((Screen.width - (width * 0.38f)), 1.1f * height, (width * 0.2f), (height * 0.5f)), "X"))
            //{
            //    displayDialog = false;
            //    wind.SetActive(false);
            //}
        }
    }
}

