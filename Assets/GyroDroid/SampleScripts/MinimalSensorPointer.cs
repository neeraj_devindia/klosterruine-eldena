// prefrontal cortex -- http://prefrontalcortex.de
// Full Android Sensor Access for Unity3D
// Contact:
// 		contact@prefrontalcortex.de

using UnityEngine;
using System.Collections;

public class MinimalSensorPointer : MonoBehaviour
{

	// Use this for initialization
	void Start () {
		// you can use the API directly:
		// Sensor.Activate(Sensor.Type.RotationVector);
		
		// or you can use the SensorHelper, which has built-in fallback to less accurate but more common sensors:
		SensorHelper.ActivateRotation();

		useGUILayout = false;
	}
	
	// Update is called once per frame
	void Update () {
       // if (UserOption.isMini_Map)
        if (true)
        {

            // Helper with fallback:
            Quaternion qt = new Quaternion(0f, SensorHelper.rotation.y, 0f, SensorHelper.rotation.w);
            transform.rotation = qt;
        }
	}
}