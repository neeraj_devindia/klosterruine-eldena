﻿using UnityEngine;
using System.Collections;

public class Explore_Audio : MonoBehaviour {

	// Use this for initialization
    private bool isSound = true;
    public Texture2D optionImg;
    public bool isPortrait = true;
    void Start()
    {
        isPortrait = System.Convert.ToBoolean(PlayerPrefs.GetInt("isPort"));

        if (isPortrait)
            Screen.orientation = ScreenOrientation.Portrait;
        else
            Screen.orientation = ScreenOrientation.Landscape;

        optionImg = Resources.Load("rotate-icon") as Texture2D;
        isSound = System.Convert.ToBoolean(PlayerPrefs.GetInt("isSound"));
	
	}
	
	// Update is called once per frame
	void Update () {
        isPortrait = System.Convert.ToBoolean(PlayerPrefs.GetInt("isPort"));

        if (isPortrait)
            Screen.orientation = ScreenOrientation.Portrait;
        else
            Screen.orientation = ScreenOrientation.Landscape;
	}
    void OnGUI()
    {
        if (isSound)
        {
            optionImg = Resources.Load("sound") as Texture2D;
            AudioListener.pause = false;
        }
        else
        {
            optionImg = Resources.Load("mute") as Texture2D;
            AudioListener.pause = true;
        }
        if (GUI.Button(new Rect((Screen.width - optionImg.width) - 10f, 5f, optionImg.width, optionImg.height), "", new GUIStyle()))
        {
            isSound = !isSound;
            PlayerPrefs.SetInt("isSound", System.Convert.ToInt16(isSound));
        }
        GUI.DrawTexture(new Rect((Screen.width - optionImg.width) - 10f, 5f, optionImg.width, optionImg.height), optionImg);


    }
}

