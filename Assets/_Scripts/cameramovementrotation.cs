﻿using UnityEngine;
using System.Collections;

public class cameramovementrotation : MonoBehaviour
{

    // Use this for initialization
    float speed = 80f;

    public GameObject target;
    void Start()
    {
        bool isPortrait = System.Convert.ToBoolean(PlayerPrefs.GetInt("isPort"));
        if (isPortrait)
        {
            camera.fieldOfView = 152;
        }
        else
        {
            camera.fieldOfView = 138;
        }
    }

    // Update is called once per frame
    float movementX;
    float movementY;

    void Update()
    {
     

        if (Input.GetMouseButton(0))
        {
            //if (Input.touchCount == 1)
            //{

            movementX = Input.GetAxis("Mouse X");
            movementY = Input.GetAxis("Mouse Y");
            //movementX = Input.touches[0].deltaPosition.x;
            //movementY = Input.touches[0].deltaPosition.y;
            if (Mathf.Abs(movementX) > Mathf.Abs(movementY))
            {
                if (movementX < 0)
                {
                    transform.RotateAround(target.transform.position, -target.transform.up, speed * Time.deltaTime);
                }
                if (movementX > 0)
                {
                    transform.RotateAround(target.transform.position, target.transform.up, speed * Time.deltaTime);
                }
            }
            else
            {

                if (movementY < 0)
                {

                    transform.RotateAround(target.transform.position, transform.right, speed * Time.deltaTime);

                    if (transform.eulerAngles.x > 70)
                    {
                        transform.RotateAround(target.transform.position, -transform.right, speed * Time.deltaTime);
                    }


                }
                if (movementY > 0)
                {
                    transform.RotateAround(target.transform.position, -transform.right, speed * Time.deltaTime);

                    if (transform.eulerAngles.x < 14)
                    {
                        transform.RotateAround(target.transform.position, transform.right, speed * Time.deltaTime);
                    }
                }
            }
        }

    }
}
