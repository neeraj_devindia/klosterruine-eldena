﻿using UnityEngine;
using System.Collections;

public class MapScript : MonoBehaviour {

    private Renderer maprender;
    public Texture2D optionImg;
    public static bool isRotateScene=true;
    //public rotating rotateCom;
    public MoveToPoint moveCom;

    public static bool isSound = true;
    public bool isPortrait = true;

    void Awake()
    {
        optionImg = Resources.Load("rotate-icon") as Texture2D;
        maprender = renderer;

       // rotateCom = GetComponent<rotate>();
        moveCom = GetComponent<MoveToPoint>();
    }
	// Use this for initialization
	void Start() 
    {
        maprender.material.mainTexture = Resources.Load("Eldena") as Texture2D;       
        maprender.enabled = true;
       
        isSound = System.Convert.ToBoolean(PlayerPrefs.GetInt("isSound"));
        isRotateScene = System.Convert.ToBoolean(PlayerPrefs.GetInt("isRotate"));

        isPortrait = System.Convert.ToBoolean(PlayerPrefs.GetInt("isPort"));

        if (isPortrait)
            Screen.orientation = ScreenOrientation.Portrait;
        else
            Screen.orientation = ScreenOrientation.Landscape;
	}
	
	// Update is called once per frame
	void Update () 
    {
      /*  isSound = System.Convert.ToBoolean(PlayerPrefs.GetInt("isSound"));
        isRotateScene = System.Convert.ToBoolean(PlayerPrefs.GetInt("isRotate"));*/
        isPortrait = System.Convert.ToBoolean(PlayerPrefs.GetInt("isPort"));
        if (isPortrait)
            Screen.orientation = ScreenOrientation.Portrait;
        else
            Screen.orientation = ScreenOrientation.Landscape;
	}
    void OnGUI()
    {
        /*
        if (isRotateScene)
        {
            optionImg = Resources.Load("rotate-icon") as Texture2D;
        }
        else
        {
            optionImg = Resources.Load("move-icon") as Texture2D;
        }
        if (GUI.Button(new Rect(10f, 5f, optionImg.width * 3f, optionImg.height * 3f), "", new GUIStyle()))
        {
            isRotateScene = !isRotateScene;            
            PlayerPrefs.SetInt("isRotate", System.Convert.ToInt16(isRotateScene));
        }
        GUI.DrawTexture(new Rect(10f, 5f, optionImg.width * 3f, optionImg.height * 3f), optionImg); */

        //________________________________________________________________________________________________________________________________________

        if (isSound)
        {
            optionImg = Resources.Load("sound") as Texture2D;
            AudioListener.pause = false;
        }
        else
        {
            optionImg = Resources.Load("mute") as Texture2D;
            AudioListener.pause = true;
        }
        if (GUI.Button(new Rect((Screen.width - optionImg.width) - 10f, 5f, optionImg.width, optionImg.height), "", new GUIStyle()))
        {
            isSound = !isSound;
            PlayerPrefs.SetInt("isSound", System.Convert.ToInt16(isSound));
        }
        GUI.DrawTexture(new Rect((Screen.width - optionImg.width) - 10f, 5f, optionImg.width, optionImg.height), optionImg);

        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}
